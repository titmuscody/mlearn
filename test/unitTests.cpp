#include "gtest/gtest.h"

#include "ClassifierTest.cpp"

TEST(INIT_TEST, PASS_TEST) {
	EXPECT_EQ(1, 1);
}
TEST(INIT_TEST, FAIL_TEST) {
	EXPECT_EQ(1, 2);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
