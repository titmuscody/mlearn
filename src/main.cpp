#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

#include "NumberClassifier.h"

using namespace cv;
using std::string;
using std::ifstream;
using std::cout;
using std::endl;
using std::ios;


int ReverseInt (int i)
{
	unsigned char ch1, ch2, ch3, ch4; ch1 = i & 255;
	ch2 = (i >> 8) & 255;
	ch3 = (i >> 16) & 255;
	ch4 = (i >> 24) & 255;
	return((int) ch1 << 24) + ((int)ch2 << 16) + ((int)ch3 << 8) + ch4;
}

void read_Mnist(string filename, vector<cv::Mat> &vec)
{
	ifstream file(filename.c_str(), ios::binary);
	if (file.is_open())
	{
		int magic_number = 0;
		int number_of_images = 0;
		int n_rows = 0;
		int n_cols = 0;
		file.read((char*) &magic_number, sizeof(magic_number));
		magic_number = ReverseInt(magic_number);
		file.read((char*) &number_of_images,sizeof(number_of_images));
		number_of_images = ReverseInt(number_of_images);
		file.read((char*) &n_rows, sizeof(n_rows));
		n_rows = ReverseInt(n_rows);
		file.read((char*) &n_cols, sizeof(n_cols));
		n_cols = ReverseInt(n_cols);
		for(int i = 0; i < number_of_images; ++i)
		{
			cv::Mat tp = Mat::zeros(n_rows, n_cols, CV_8UC1);
			for(int r = 0; r < n_rows; ++r)
			{
				for(int c = 0; c < n_cols; ++c)
				{
					unsigned char temp = 0;
					file.read((char*) &temp, sizeof(temp));
					tp.at<uchar>(r, c) = (int) temp;
				}
			}
			vec.push_back(tp);
		}
	}
}

void read_Mnist_Label(string filename, vector<double> &vec)
{
	ifstream file(filename.c_str(), ios::binary);
	if (file.is_open())
	{
		int magic_number = 0;
		int number_of_images = 0;
		int n_rows = 0;
		int n_cols = 0;
		file.read((char*) &magic_number, sizeof(magic_number));
		magic_number = ReverseInt(magic_number);
		file.read((char*) &number_of_images,sizeof(number_of_images));
		number_of_images = ReverseInt(number_of_images);
		for(int i = 0; i < number_of_images; ++i)
		{
			unsigned char temp = 0;
			file.read((char*) &temp, sizeof(temp));
			vec[i]= (double)temp;
		}
	}
}

int main(int argc, char** argv)
{
	srand(time(NULL));
	//read in training images
	string filename = "mnist/train-images-idx3-ubyte";
	vector<cv::Mat> images;
	read_Mnist(filename, images);
	cout<<images.size()<<endl;
	cout << images[0].size() << endl;
	
	//read in training labels
	filename = "mnist/train-labels-idx1-ubyte";
	int number_of_images = 60000;
	vector<double> labels(number_of_images);
	read_Mnist_Label(filename, labels);
	cout<<labels.size()<<endl;
	
	//train classifier
	NumberClassifier c;
	for(int i = 0; i < labels.size(); i++)
	{
		c.Train(images[i], labels[i]);
	}
	//test classifier
	namedWindow("view", WINDOW_NORMAL);
	int count = 0;
	int right = 0;
	for(int i = 0; i < 15; ++i)
	{
		count += 1;
		int num = rand() % labels.size();
		while(labels[num] != 1)
		{
			if(num == labels.size())
				num = 0;
			++num;
		}
		cout << "actual " << labels[num] << endl;
		int guess = c.Predict(images[num]);
		cout << "guess " << guess << endl;
		if(labels[num] == guess)
			right += 1;
		imshow("view", images[num]);
		//waitKey();
	}
	cout << "guessed " << right << " out of " << count << endl;
	return 0;
}
