
#include <opencv2/opencv.hpp>

#include "Classifier.h"

using cv::Mat;

class NumberClassifier {
private:
	Classifier one;

public:
	void Train(Mat image, int value);
	int Predict(Mat image);
};

void NumberClassifier::Train(Mat image, int value)
{
	one.Train(image, value);
}

int NumberClassifier::Predict(Mat image)
{
	return one.Predict(image);
}
