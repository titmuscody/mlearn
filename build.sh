#!/bin/bash

set -e

cmake -H. -Bbuild
pushd build
make
./runTests
./predict
make clean
popd
rm -R build
